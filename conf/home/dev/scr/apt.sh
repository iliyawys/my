#!/bin/bash
case $1 in 

1 ) apt-get update && apt-get upgrade ;;
2 ) apt-get install wireless-tools aircrack-ng wicd-gtk wpasupplicant hunt firmware-realtek;;
3 ) apt-get install xserver-xorg xinit awesome awesome-extra;;
4 ) apt-get install mc nano vim firefox-esr w3m rxvt-unicode  alsa-base alsa-tools alsa-utils deborphan feh flashplugin-nonfree ftp fuse irssi  sudo  nmap nethack-console  newsbeuter rar zip unrar unzip vlc moc weechat urlview screen xprop;;
5 ) apt-get install apache2 apache2-utils php5 medusa  mysql-server git-gui gitk sublime text ssh;;
* ) echo "1) update\n2) networks\n3) xorg \n4) env\n5) dev";;
esac
