<?php

    namespace app\components;

    class Captcha
    {
        public static $domain = 'antigate.com';
        public static $key = false;
        public static $method = 'base64';
        public static $repeat_interval = 5;
        public static $timeout = 30;
        public static $transport = 'curl';
        public static $debug = false;

        public static function __init($params=array()){
            if (isset($params['domain'])){
                self::$domain = $params['domain'];
            }
            if (isset($params['method'])){
                if(in_array($params['method'],array('post','base64'))){
                    self::$method = $params['method'];
                }
                else{
                    echo "invalid params: method\n";
                    return false;
                }
            }
            if (isset($params['transport'])){
                if(in_array($params['transport'],array('curl','file_get_contents'))){
                    self::$transport = $params['transport'];
                }
                else{
                    echo "invalid params: transport\n";
                    return false;
                }
            }
            if (isset($params['key'])){
                if(!self::$key || self::$key == '' || strlen(self::$key) != 32){
                    self::$key = $params['key'];
                }
                else{
                    echo "invalid params: key\n";
                    return false;
                }
            }
            if (isset($params['debug'])){
                self::$debug = true;
            }

            return true;
        }

        /**
         * @param $source (string) $filename || base64-encoded string
         * @param $params (array)
         */
        public static function recognize($source,$params=array()){
            $repeat_interval 	= isset($params['repeat_interval']) ? $params['repeat_interval'] : 5;
            $debug 				= isset($params['debug']) ? true : self::$debug;

            $res = self::upload($source,$params);
            if (!$res){
                echo "error: could not upload image";
                return false;
            }

            if ($debug) echo "waiting for $repeat_interval seconds\n";
            sleep($repeat_interval);

            return self::result($res,$params);
        }

        /**
         * @param $source (string) $filename || base64-encoded string
         * @param $params (array)
         */
        public static function upload($source,$params=array()){
            //main params
            $domain 			= isset($params['domain']) ? $params['domain'] : self::$domain;
            $method 			= isset($params['method']) && in_array($params['method'],array('post','base64')) ? $params['method'] : self::$method;
            $transport 			= isset($params['transport']) && in_array($params['transport'],array('curl','file_get_contents')) ? $params['transport'] : self::$transport;

            //extra params
            $phrase 			= isset($params['phrase']) ? $params['phrase'] & 1 : 0;
            $regsense 			= isset($params['regsense']) ? $params['regsense'] & 1 : 0;
            $numeric 			= isset($params['numeric']) ? $params['numeric'] & 1 : 0;
            $is_russian 		= isset($params['is_russian']) ? $params['is_russian'] & 1 : 0;
            $language 			= isset($params['language']) ? intval($params['language']) : 0;
            $min_len			= isset($params['min_len']) ? intval($params['min_len']) : 0;
            $max_len			= isset($params['max_len']) ? intval($params['max_len']) : 0;
            $soft_id			= isset($params['soft_id']) ? intval($params['soft_id']) : 0;

            $debug 				= isset($params['debug']) ? true : self::$debug;

            if ($method == 'post' && !file_exists($source)){
                echo "invalid params: image file not found\n";
                return false;
            }
            if (!isset(self::$key) || self::$key == '' || strlen(self::$key) != 32){
                echo "invalid params: key\n";
                return false;
            }

            $request_url = 'http://'.$domain.'/in.php';

            $post_vars = array(
                'method'    => $method,
                'phrase'	=> $phrase,
                'regsense'	=> $regsense,
                'numeric'	=> $numeric,
                'is_russian'=> $is_russian,
                'language'	=> $language,
                'min_len'	=> $min_len,
                'max_len'	=> $max_len,
                'soft_id'	=> $soft_id,
            );

            if ($method == 'post'){
                if ($transport == 'curl'){
                    $post_vars['file'] = '@'.$source;
                }
                else{
                    $post_vars['file'] = $source;
                }
            }
            else{
                $post_vars['body'] = $source;
            }

            $request_params = array(
                'postfields' => $post_vars,
                'transport' => $transport,
            );

            $res = self::__request($request_url,$request_params);
            if (!$res){
                if ($debug) echo "error: could not perform request";
                return false;
            }

            $ex = explode("|", $res);
            if (strpos($res, 'ERROR')!==false){
                if ($debug) echo "error: $res\n";
                return $res;
            }

            $captcha_id = trim($ex[0])=='OK' && isset($ex[1]) ? trim($ex[1]) : false;
            if (!$captcha_id || !preg_match('@^[\d]+$@',$captcha_id)){
                if ($debug) echo "error: could not get captcha ID\n";
                return false;
            }

            if ($debug) echo "captcha sent, got captcha ID ".$captcha_id."\n";

            return $captcha_id;
        }

        /**
         * @param $captcha_id (int) || $captcha_ids (array)
         * @param $params (array)
         */
        public static function result($captcha_ids,$params=array()){
            //main params
            $domain 			= isset($params['domain']) ? $params['domain'] : self::$domain;
            $transport 			= isset($params['transport']) && in_array($params['transport'],array('curl','file_get_contents')) ? $params['transport'] : self::$transport;
            $repeat_interval 	= isset($params['repeat_interval']) ? $params['repeat_interval'] : 5;
            $timeout 			= isset($params['timeout']) ? $params['timeout'] : 30;
            $debug 				= isset($params['debug']) ? true : self::$debug;

            if (!isset($captcha_ids)){
                echo "invalid params: captcha id\n";
                return false;
            }

            if (!is_array($captcha_ids)){
                $wtime = 0;
                while(true){
                    $request_url = 'http://'.$domain.'/res.php?action=get&id='.$captcha_ids;
                    $res = self::__request($request_url,array('transport' => $transport));
                    if (!$res){
                        if ($debug) echo "error: could not perform request";
                        return false;
                    }

                    if (strpos($res, 'ERROR')!==false){
                        if ($debug) echo "error: $res\n";
                        return false;
                    }
                    elseif ($res=="CAPCHA_NOT_READY" || $res=="CAPTCHA_NOT_READY"){
                        if ($debug) echo "captcha is not ready yet\n";

                        $wtime += $repeat_interval;
                        if ($wtime>$timeout){
                            if ($debug) echo "error: timeout\n";
                            return false;
                        }
                    }
                    elseif (strpos(trim($res), 'OK')!==false){
                        $ex = explode('|', $res);
                        if (trim($ex[0])=='OK'){
                            return array($captcha_ids => trim($ex[1]));
                        }
                    }
                    else{
                        if ($debug) echo "error: invalid remote response $res\n";
                        return false;
                    }

                    if ($debug) echo "waiting for $repeat_interval seconds\n";
                    sleep($repeat_interval);
                }
            }
            else{
                $request_url = 'http://'.$domain.'/res.php?action=get&ids='.implode(',',$captcha_ids);
                $res = self::__request($request_url,array('timeout' => $timeout,'transport' => $transport));
                if (!$res){
                    if ($debug) echo "error: could not perform request";
                    return false;
                }

                $ex = explode('|', $res);
                $res = array();
                $i=0;
                foreach($captcha_ids as $captcha_id){
                    $res[$captcha_id] = trim(array_shift($ex));
                }

                return $res;
            }

            if ($debug) echo "error: unknown\n";
            return false;
        }

        public static function complain($captcha_id,$params=array()){
            $domain 			= isset($params['domain']) ? $params['domain'] : self::$domain;
            $transport 			= isset($params['transport']) && in_array($params['transport'],array('curl','file_get_contents')) ? $params['transport'] : self::$transport;
            $debug 				= isset($params['debug']) ? true : self::$debug;

            if (!isset($captcha_id) || $captcha_id <= 0){
                if ($debug) echo "invalid params: captcha id\n";
                return false;
            }

            $request_url = 'http://'.$domain.'/res.php?action=reportbad&id='.$captcha_id;
            $res = self::__request($request_url,array('transport' => $transport));
            if (!$res){
                if ($debug) echo "error: could not perform request\n";
                return false;
            }

            if (strpos($res, 'ERROR')!==false){
                if ($debug) echo "error: $res\n";
                return false;
            }
            elseif (trim($res)=='OK_REPORT_RECORDED'){
                return true;
            }
            else{
                if ($debug) echo "error: invalid remote response $res\n";
                return false;
            }
        }

        public static function balance($params=array()){
            $domain 			= isset($params['domain']) ? $params['domain'] : self::$domain;
            $transport 			= isset($params['transport']) && in_array($params['transport'],array('curl','file_get_contents')) ? $params['transport'] : self::$transport;
            $debug 				= isset($params['debug']) ? true : self::$debug;

            $request_url = 'http://'.$domain.'/res.php?action=getbalance';
            $res = self::__request($request_url,array('transport' => $transport));
            if (!$res){
                if ($debug) echo "error: could not perform request";
                return false;
            }

            return $res;
        }

        public static function stats($date,$params=array()){
            $domain 			= isset($params['domain']) ? $params['domain'] : self::$domain;
            $transport 			= isset($params['transport']) && in_array($params['transport'],array('curl','file_get_contents')) ? $params['transport'] : self::$transport;
            $debug 				= isset($params['debug']) ? true : self::$debug;

            if (!isset($date) || date('Y-m-d',strtotime($date)) != $date){
                if ($debug) echo "invalid params: date\n";
                return false;
            }

            $request_url = 'http://'.$domain.'/res.php?action=getstats&date='.$date;
            $res = self::__request($request_url,array('transport' => $transport));
            if (!$res){
                if ($debug) echo "error: could not perform request";
                return false;
            }

            $res = explode('<stats',$res);
            unset($res[0]);
            $stats = array();
            foreach($res as $row){
                if (preg_match_all('@[\d.-]+@',$row,$m)){
                    $stats[] = array(
                        'dateint' => $m[0][0],
                        'date' => $m[0][1],
                        'hour' => $m[0][2],
                        'volume' => $m[0][3],
                        'money' => $m[0][4],
                    );
                }
            }

            return $stats;
        }

        private static function __request($url,$params=array())
        {
            $transport 			= isset($params['transport']) && in_array($params['transport'],array('curl','file_get_contents')) ? $params['transport'] : self::$transport;
            $debug 				= isset($params['debug']) ? true : self::$debug;
            $timeout 			= isset($params['timeout']) ? $params['timeout'] : 10;

            if (!isset($url) || $url == ''){
                if ($debug) echo 'invalid params: url';
                return false;
            }

            if (!isset(self::$key) || !self::$key || self::$key == '' || strlen(self::$key) != 32){
                echo "invalid params: key\n";
                return false;
            }

            if (strpos($url,'in.php')!==false){
                $params['postfields']['key'] = self::$key;
            }
            elseif (strpos($url,'res.php')!==false){
                $url .= '&key='.self::$key;
            }

            if ($transport == 'curl'){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

                if (isset($params['postfields'])){
                    curl_setopt ($ch, CURLOPT_POST, 1);
                    if (isset($params['postfields'])) curl_setopt ($ch, CURLOPT_POSTFIELDS, $params['postfields']);
                }

                curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

                $res = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                if ($http_status != 200){
                    if ($debug) echo "invalid HTTP status ".$http_status;
                    return false;
                }
                $curl_error_no = curl_errno($ch);
                $curl_error_description = curl_error($ch);
                if ($curl_error_no){
                    if ($debug) echo "CURL error ".$curl_error_no." (".$curl_error_description.")";
                    return false;
                }

                return $res;
            }
            elseif($transport == 'file_get_contents'){

                $context_params = array(
                    'http' => array(
                        'timeout' => $timeout,
                    )
                );

                if (isset($params['postfields'])){
                    $multipart_boundary = '--------------------------'.microtime(true);

                    $header = 'Content-Type: multipart/form-data; boundary='.$multipart_boundary;
                    $content = '';

                    if (isset($params['postfields']['file'])){
                        $res = file_get_contents($params['postfields']['file']);
                        if (!$res){
                            if ($debug) echo 'error: could not read source file';
                            return false;
                        }

                        $content =  "--".$multipart_boundary."\r\n".
                            "Content-Disposition: form-data; name=\"file\"; filename=\"".basename($params['postfields']['file'])."\"\r\n".
                            "Content-Type: application/zip\r\n\r\n".
                            $res."\r\n";

                        unset($params['postfields']['file']);
                    }


                    if (count($params['postfields'])){
                        foreach($params['postfields'] as $k => $v){
                            $content .= "--".$multipart_boundary."\r\n".
                                "Content-Disposition: form-data; name=\"$k\"\r\n\r\n".
                                "$v\r\n";
                        }
                    }

                    $content .= "--".$multipart_boundary."--\r\n";

                    $context_params['http']['method'] = 'POST';
                    $context_params['http']['header'] = $header;
                    $context_params['http']['content'] = $content;
                }


                $context = stream_context_create($context_params);
                $res = file_get_contents($url, false, $context);
                if (!$res){
                    if ($debug) echo "error: could not perform request";
                    return false;
                }

                return $res;
            }
        }
    }

?>
