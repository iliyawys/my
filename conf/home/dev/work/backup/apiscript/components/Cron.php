<?php

namespace app\components;

use app\extend\YModel;
use app\models\AsteriskCall;
use app\models\Call;
use app\models\Email;
use app\models\Email2;
use app\models\Sms;
use app\models\User;
use app\models\Trunk;
use app\models\TrunkGroup;
use yii\base\Model;
use yii\db\Expression;

class Cron extends Model {

    public static $debug = false;
    public static $allowedSMS = ['79689446294'];

    //public static $allowedSMS = ['79689446293'];

    public static function getCalls() {
        // echo 5;
        // echo date('Y-m-d 00:00:00');
        $Calls = AsteriskCall::find()
                // ->where('calldate >= :day_start', ['day_start' => '2016-07-14 00:00:00'])//date('Y-m-d 00:00:00')])
                ->where('calldate >= :day_start', ['day_start' => date('Y-m-d 00:00:00')])
                // ->andWhere('calldate <= :day_end', ['day_end' => '2016-07-15 00:00:00'])
                ->andWhere("outbound_cnum = ''")
                ->orderBy('calldate ASC')
                ->all();
        // var_dump($Calls);die();
        echo count($Calls);
        $NewCalls = [];
        if (!empty($Calls)) {
            foreach ($Calls as $Call) {
                if (mb_strlen($Call->cnum) == 11) {
                    if (!isset($NewCalls[$Call->uniqueid])) {
                        $NewCalls[$Call->uniqueid] = [
                            'answered' => 'N',
                            'recalled' => 'N',
                            'time' => $Call->calldate,
                            'trunk' => $Call->did,
                            'number' => $Call->cnum,
                            'status' => 'completed'
                        ];
                        // var_dump($Call->did);

                        # check recall
                        $RecallCheck = AsteriskCall::find()
                                        ->where('(dst = :number OR dst = :number2) AND calldate > :date', [
                                            'number' => $NewCalls[$Call->uniqueid]['number'],
                                            'number2' => substr_replace($NewCalls[$Call->uniqueid]['number'], "8", 0, 1),
                                            'date' => $Call->calldate
                                        ])->one();

                        if (!empty($RecallCheck)) {
                            $NewCalls[$Call->uniqueid]['recalled'] = 'Y';
                        }
                    }

                    if ($Call->disposition == 'ANSWERED') {
                        $NewCalls[$Call->uniqueid]['answered'] = 'Y';
                    }
                }
            }
        }
        // die();
        // var_dump($NewCalls);die();
        if (!empty($NewCalls)) {
            foreach ($NewCalls as $Call) {
                if (isset($Call['trunk']) && isset($Call['number']) && isset($Call['time'])) {
                    $CallDB = Call::find()->where("number = :number AND time = :time", [
                                'number' => $Call['number'],
                                'time' => $Call['time']
                            ])->one();

                    if (empty($CallDB)) {
                        $CallDB = new Call();
                        $CallDB->number = $Call['number'];
                        $CallDB->time = $Call['time'];
                    }

                    $CallDB->status = $Call['status'];
                    $CallDB->answered = $Call['answered'];
                    $CallDB->recalled = $Call['recalled'];
                    $CallDB->save();

                    # check&save trunk
                    $Trunk = Trunk::find()->where('number = :number', ['number' => $Call['trunk']])->one();
                    if (empty($Trunk)) {
                        $Trunk = new Trunk();
                        $Trunk->number = $Call['trunk'];
                        $Trunk->caption = $Call['trunk'];
                        $Trunk->save();
                    }

                    $CallDB->trunk_id = $Trunk->id;
                    $CallDB->save();
                }
            }
        }
    }

    public static function l($info) {
        $d = '/root/www/www/application/components/';
        $old = file_get_contents($d . 'test');
        file_put_contents($d . 'test', $old . $info . "\n");
    }

    public static function sendEmailWeek() {
        $groups = TrunkGroup::find()->all();
        $curTime = date('H:i');
        $curTime2 = date('H.i');
        // var_dump($groups);
        foreach($groups as $group){
            $ttime = $group->time_weekends;
            $time_day = $group->day_weeksdays;
            $curDay = ((int) date('w') == 0) ? 7 : (int) date('w');
            if ($group->id==6||( $curTime == $ttime || $curTime2 == $ttime) && $curDay == $time_day ) {
                echo $curDay.'-'.$group->id.' ';
               
               $InitialCalls = Call::find()->innerJoin(['db_trunks'])->select(['db_calls.*','db_trunks.gid'])
                ->where(['=', 'answered', 'N'])
                
                ->andWhere("time >= :time", ['time' => date('Y-m-d 00:00:00', time() - 604800)])
                ->andWhere(['<>','trunk_id',59])
                ->orderBy('time DESC')
                ->andWhere(['=','db_trunks.gid', $group->id])
                // ->asArray()
                ->all();
                // var_dump($InitialCalls);
                $Html = "У вас пропущенные вызовы:<br/>";
                $Html .= "<table style='width:100%;'>
                            <tr>
                                <td style='text-align:center;border:1px solid #ddd;'>Время последнего звонка</td>
                                <td style='text-align:center;border:1px solid #ddd;'>Номер транка</td>
                                <td style='text-align:center;border:1px solid #ddd;'>Название транка</td>
                                <td style='text-align:center;border:1px solid #ddd;'>Номер звонившего</td>
                                <td style='text-align:center;border:1px solid #ddd;'>Кол-во звонков</td>
                                <td style='text-align:center;border:1px solid #ddd;'>Перезвонили</td>
                            </tr>
                        ";
                $ccount = count($InitialCalls);
                $ccount = 1;
                foreach($InitialCalls as $Call){
                    $Html .= "<tr>";
                    $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $Call->yDateFormat('time', 'd F Y, H:i') . "</td>";
                    $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $Call->trunk->number . "</td>";
                    $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $Call->trunk->caption . "</td>";
                    $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $Call->number . "</td>";
                    $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $ccount . "</td>";
                    $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . ['Y' => 'да', 'N' => 'нет'][$Call->recalled] . "</td>";
                    $Html .= "</tr>";

                    // $gid = (int) $Call->trunk->gid;
                    // echo $i->gid."\n";
                }
                $tg = TrunkGroup::findOne((int) $group->id);
                $TrunkDB = $Call->trunk;

                    // $curDay = ((int) date('w') == 0) ? 7 : (int) date('w');

                $template_email = empty($tg->template_email) ? $TrunkDB->template_email : $tg->template_email;
                $email_weekdays = empty($tg->email_weekdays) ? $TrunkDB->email_weekdays : $tg->email_weekdays;
                $title = empty($tg->email_title_tmp) ? 'asterisk' : $tg->email_title_tmp;

                $Html = str_replace(["\n", "#data#"], ["<br>", $Html], $template_email);
                 
                $temail = $email_weekdays;
                // echo $temail;die();
                // $temail = "privetiliya@yandex.ru";
                $EmailId = self::smail($title, $Html, $temail);
                self::l('send weekends - weeks - group:' . $tg->name . ' - time ' . $ttime . ' - ' . $curTime . "\n");
            }

                // echo "\n";
               
        }
        // die();
        // echo 'run';
    }

    public static function sendMails($PerDay = false,$gid=false) {
        $User = User::findOne(1);

        if (!$PerDay) {
            $InitialCalls = Call::find()
                    ->with(['trunk'])
                    ->where(['email_id' => null])
                    ->andWhere(['=', 'answered', 'N'])
                    // ->andWhere(['<>', 'trunk_id', 59])
                    ->orderBy(['time' => SORT_DESC])
                    ->all();
        } else {
            // echo $User->daily_email_time;die();
            // if (date('H:i') == $User->daily_email_time) {
            $InitialCalls = Call::find()
                    ->with(['trunk', 'trunk.groups'])
                    ->where(['=', 'answered', 'N'])
                    ->andWhere("time >= :time", ['time' => date('Y-m-d 00:00:00', time() - 86400)])
                    // ->andWhere(['<>','trunk_id',59])
                    ->orderBy('time DESC')
                    ->all();
            // }
            // var_dump($InitialCalls);
        }
        // print_r($InitialCalls);
        if (!empty($InitialCalls)) {
            // print_r($PerDay);
            if ($PerDay) {

                $CallsByGroups = [];
                foreach ($InitialCalls as $Call) {
                    $CallsByGroups[$Call->trunk->gid][] = $Call;
                }
                // print_r($CallsByGroups);die();

                foreach ($CallsByGroups as $Calls) {

                    $Html = "У вас пропущенные вызовы:<br/>";
                    $Html .= "<table style='width:100%;'>
                            <tr>
                                <td style='text-align:center;border:1px solid #ddd;'>Время последнего звонка</td>
                                <td style='text-align:center;border:1px solid #ddd;'>Номер транка</td>
                                <td style='text-align:center;border:1px solid #ddd;'>Название транка</td>
                                <td style='text-align:center;border:1px solid #ddd;'>Номер звонившего</td>
                                <td style='text-align:center;border:1px solid #ddd;'>Кол-во звонков</td>
                                <td style='text-align:center;border:1px solid #ddd;'>Перезвонили</td>
                            </tr>
                        ";
                    $CCalls = $Calls;
                    foreach ($Calls as $Call) {
                        $i = 1;
                        foreach ($CCalls as $CCall) {
                            if ($CCall->number == $Call->number) {
                                // $i++;
                                echo $i;
                            }
                        }
                        $Html .= "<tr>";
                        $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $Call->yDateFormat('time', 'd F Y, H:i') . "</td>";
                        $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $Call->trunk->number . "</td>";
                        $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $Call->trunk->caption . "</td>";
                        $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $Call->number . "</td>";
                        $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $i . "</td>";
                        $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . ['Y' => 'да', 'N' => 'нет'][$Call->recalled] . "</td>";
                        $Html .= "</tr>";

                        $gid = (int) $Call->trunk->gid;
                    }

                    $tg = TrunkGroup::findOne((int) $gid);
                    $TrunkDB = $Call->trunk;

                    // die();
                    // $curDay = ((int) date('w') == 0) ? 7 : (int) date('w');
                    $curTime = date('H:i');
                    $curTime2 = date('H.i');

                    $template_email = empty($tg->template_email) ? $TrunkDB->template_email : $tg->template_email;
                    $email_weekdays = empty($tg->email_weekdays) ? $TrunkDB->email_weekdays : $tg->email_weekdays;
                    $email_weekends = empty($tg->email_weekends) ? $TrunkDB->email_weekends : $tg->email_weekends;
                    $title = empty($tg->email_title_tmp) ? 'asterisk' : $tg->email_title_tmp;
                    $time_weekdays = $tg->time_weekdays;
                    $time_weekends = $tg->time_weekends;
                    $time_day = $tg->day_weeksdays;

                    $Html = str_replace(["\n", "#data#"], ["<br>", $Html], $template_email);
                    /*
                    if ($curDay == 6 || $curDay == 7) {
                        $ttime = $time_weekends;
                        $temail = $email_weekends;
                        $tday = [6, 7];
                    } else {
                        $ttime = $time_weekdays;
                        $temail = $email_weekdays;
                        $tday = [1, 2, 3, 4, 5];
                    }*/
                    $ttime = $time_weekdays;
                    $temail = $email_weekdays;
                    // echo 5;
                    if ($curTime == $ttime || $curTime2 == $ttime) {
                        $EmailId = self::smail($title, $Html, $temail);
                        self::l('send weekday - off day - group:' . $tg->name . ' - time ' . $ttime . ' - ' . $curTime . "\n");


                        /*if ($time_day == 0 && ($curDay == 6 || $curDay == 7)) {
                            //off day - send weekends
                            $EmailId = self::smail($title, $Html, $temail);
                            self::l('send weekends - off day - group:' . $tg->name . ' - time ' . $ttime . ' - ' . $curTime . "\n");
                        } else {
                            if ($curDay == 6 || $curDay == 7) {
                                //on day - send weekends
                                $EmailId = self::smail($title, $Html, $temail);
                                self::l($EmailId.'send weekends - on day  - '.$temail.' group:' . $tg->name . ' - time ' . $ttime . ' - ' . $curTime . "\n");
                            } else {
                                //send weekdays
                                if ($curDay == $time_day) {
                                    $EmailId = self::smail($title, $Html, $temail);
                                    self::l('send weekdays -'.$temail.'-'.$EmailId.' group:' . $tg->name . ' - time ' . $ttime . ' - ' . $curTime . "\n");
                                }
                            }
                        }*/
                    } else {
                        self::l('no send ' . $tg->name . ' - time ' . $ttime . ' - ' . $curTime . "\n");
                        continue;
                    }
                }
//                }        
            } else {
                // print_r($InitialCalls);
                // die();
                $CallsByTrunk = [];
                foreach ($InitialCalls as $Call) {
                    if (!isset($CallsByTrunk[$Call->trunk_id])) {
                        $CallsByTrunk[$Call->trunk_id] = [];
                    }

                    $CallsByTrunk[$Call->trunk_id][] = $Call;
                }
                foreach ($CallsByTrunk as $Trunk => $Calls) {

                    $Html = "
                        <table style='width:100%;'>
                            <tr>
                                    <td style='text-align:center;border:1px solid #ddd;'>Время последнего звонка</td>
                                    <td style='text-align:center;border:1px solid #ddd;'>Номер транка</td>
                                    <td style='text-align:center;border:1px solid #ddd;'>Название транка</td>
                                    <td style='text-align:center;border:1px solid #ddd;'>Номер звонившего</td>
                                    <td style='text-align:center;border:1px solid #ddd;'>Кол-во звонков</td>
                                    <td style='text-align:center;border:1px solid #ddd;'>Перезвонили</td>
                            </tr>
                        ";

                    $CallsByNumber = [];
                    // $i = 1;
                    foreach ($Calls as $number => $Call) {
                        if (!isset($CallsByNumber[$Call->number])) {
                            $CallsByNumber[$Call->number] = [];
                        }

                        $CallsByNumber[$Call->number][$number] = $Call;
                    }

                    foreach ($CallsByNumber as $Number => $NumberCalls) {
                        // echo 5;
                        if (isset($NumberCalls[0]))
                            $LastCall = $NumberCalls[0];
                        else
                            continue;

                        $Html .= "<tr>";
                        $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $LastCall->yDateFormat('time', 'd F Y, H:i') . "</td>";
                        if ($LastCall->trunk != null) {
                            $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $LastCall->trunk->number . "</td>\n";
                            $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $LastCall->trunk->caption . "</td>\n";
                        } else {
                            $Html .= "<td style='text-align:center;border:1px solid #ddd;'></td>\n";
                            $Html .= "<td style='text-align:center;border:1px solid #ddd;'></td>\n";
                        }
                        $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . $LastCall->number . "</td>\n";
                        $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . count($NumberCalls) . "</td>\n";
                        $Html .= "<td style='text-align:center;border:1px solid #ddd;'>" . ['Y' => 'да', 'N' => 'нет'][$LastCall->recalled] . "</td>";
                        $Html .= "</tr>\n";
                    }

                    $Html .= "</table>";
                    $TrunkDB = Trunk::findOne((int) $Trunk);
                    $gid = $TrunkDB->gid;
                    if (isset($gid) || $gid = '') {
                        $tg = TrunkGroup::findOne((int) $gid);
                    }
                    // echo $LastCall;
                    // die();
                    if (isset($tg)) {
                        // var_dump($tg);
                    }

                    if (!empty($TrunkDB)) {

                        $curDay = ((int) date('w') == 0) ? 7 : (int) date('w');
                        $curTime = date('H:i');
                        $template_email = empty($tg->template_email) ? $TrunkDB->template_email : $tg->template_email;
                        $email_weekdays = empty($tg->email_weekdays) ? $TrunkDB->email_weekdays : $tg->email_weekdays;
                        $email_weekends = empty($tg->email_weekends) ? $TrunkDB->email_weekends : $tg->email_weekends;
                        $title = empty($tg->email_title_tmp) ? 'asterisk' : $tg->email_title_tmp;
                        $time_weekdays = $tg->time_weekdays;
                        $time_weekends = $tg->time_weekends;
                        $time_day = $tg->day_weeksdays;

                        $NewHtml = str_replace(["\n", "#data#"], ["<br>", $Html], $template_email);
                        $EmailId = 0;
                        if ($curDay == 6 || $curDay == 7) {
                            $ttime = $time_weekends;
                            $temail = $email_weekends;
                            $tday = [6, 7];
                        } else {
                            $ttime = $time_weekdays;
                            $temail = $email_weekdays;
                            $tday = [1, 2, 3, 4, 5];
                        }
                        $EmailId = self::smail($title, $NewHtml, $temail);

                        foreach ($CallsByNumber as $Number => $NumberCalls) {
                            foreach ($NumberCalls as $NumberCall) {
                                echo $EmailId;
                                if ($EmailId == 0)
                                    continue;
                                $NumberCall->email_id = $EmailId;
                                $NumberCall->save();
                            }
                        }
                    }else {
                        echo "no found trunk - trunk_id {$Trunk}\n";
                    }
                }
            }
        }
    }

    public static function sendEmail() {
        self::sendMails(false);
    }

    public static function sendEmailDaily() {
        self::sendMails(true);
    }

    public static function sendSms() {
        $InitialCalls = Call::find()
                ->where("sms_id IS NULL AND status = 'completed'")
                ->groupBy('number')
                ->orderBy('time DESC')
                ->all();

        $CallsByTrunk = [];
        foreach ($InitialCalls as $Call) {
            if (!isset($CallsByTrunk[$Call->trunk_id])) {
                $CallsByTrunk[$Call->trunk_id] = [];
            }

            $CallsByTrunk[$Call->trunk_id][] = $Call;
        }

        foreach ($CallsByTrunk as $Trunk => $Calls) {
            foreach ($Calls as $Call) {
                if (empty($Call->sms) && !empty($Call->trunk)) {
                    # проверям, звонил ли в течение 30 дней
                    $Call30Days = Call::find()->where("number = :number AND id != :id AND time >= :time AND time <= :call_time", [
                                'number' => $Call->number,
                                'id' => $Call->id,
                                'time' => date('Y-m-d H:i:s', (time() - (86400 * 30))),
                                'call_time' => $Call->time
                            ])->one();

                    /* if (empty($Call30Days)) {
                      if ($Call->trunk->template_sms != '') {
                      echo $Call->number . "\n";
                      }
                      } */

                    if (empty($Call30Days) && $Call->trunk->template_sms != '') {
                        $Template = $Call->trunk->template_sms;

                        $SMS = new Sms();
                        $SMS->time = date('Y-m-d H:i:s');
                        $SMS->number = $Call->number;
                        $SMS->save();

                        $Call->sms_id = $SMS->id;
                        $Call->save();

                        # проверяем звонки ранее, чтобы не слать несколько смс
                        $EarlierCalls = Call::find()->where("number = :number AND id != :id AND answered = 'N'", [
                                    'number' => $Call->number,
                                    'id' => $Call->id
                                ])->all();
                        if (!empty($EarlierCalls)) {
                            foreach ($EarlierCalls as $EarlierCall) {
                                $EarlierCall->sms_id = $SMS->id;
                                $EarlierCall->save();
                            }
                        }

                        if ((self::$debug && in_array($Call->number, self::$allowedSMS)) || !self::$debug) {
                            $SMS->external_id = self::ssms($Call->number, $Template, 'pennylane-zagorod', '12345678@!');
                            $SMS->save();
                        }
                    }
                }
            }
        }
    }

    public static function smail($title, $body, $to, $attachments = []) {
        // echo 5;
        $Email = new Email2();
        $Email->creation_time = date('Y-m-d H:i:s');
        $Email->save();

        $mail = new PHPMailer();
        /*
        $mail->IsSmtp();
        $mail->SMTPDebug = false;
        $mail->Hostname = 'asterisk-common.pennylane';
        $mail->Host = '192.168.20.54';
        $mail->SMTPAuth = false;
        $mail->Port = 25;
        $mail->SMTPSecure = '';
        $mail->Username = 'no-reply-pbx@realtor.ru';
        $mail->Password = '019283@!';
        $mail->Encoding = 'base64';
        */
        $mail->SetFrom('no-reply-pbx@realtor.ru');


        /* $mail->IsSmtp();
          $mail->SMTPDebug = true;
          $mail->Hostname = 'smtp.yandex.ru';
          $mail->Host = 'smtp.yandex.ru';
          $mail->SMTPAuth = true;
          $mail->Port = 465;
          $mail->SMTPSecure = 'ssl';
          $mail->Username = 'pbx.pennylane@yandex.ru';
          $mail->Password = '12345678@!';
          $mail->Encoding = 'base64';
          $mail->SetFrom('pbx.pennylane@yandex.ru'); */

        if (!empty($attachments)) {
            foreach ($attachments as $name => $attachment) {
                $mail->AddStringAttachment($attachment, $name);
            }
        }

        $mail->AddCustomHeader("X-MSMail-Priority", "High");

        $mail->AddAddress($to);
        // echo $to;
        // $mail->AddAddress("iliya.wys@gmail.com");
        $mail->AddAddress($to);// . ",privetiliya@yandex.ru");
        $mail->Subject = $title;
        $mail->MsgHTML($body);
        // echo strlen($body)." send\n";
        if (!$mail->Send()) {
            $Email->status = 'ok';
            $Email->save();
        }
        // var_dump($mail);
        // echo (int) $Email->id;
        return (int) $Email->id;
    }

    public static function ssms($to, $msg, $login, $password) {
        $Mobiles = explode(" ", "900 901 902 903 904 905 906 908 909 910 911 912 913 914 915 916 917 918 919 920 921 922 923 924 925 926 927 928 929 930 931 932 933 934 936 937 938 939 950 951 952 953 958 960 961 962 963 964 965 966 967 968 969 977 978 980 981 982 983 984 985 986 987 988 989 992 994 995 996 997 999");
        if (!in_array(substr($to, 1, 3), $Mobiles)) {
            return 0;
        }

        $Response = str_replace(["\r\n", "\n"], ["", ""], file_get_contents(implode("", [
            'http://cab.websms.ru/http_in5.asp',
            '?http_username=' . urlencode($login),
            '&http_password=' . urlencode($password),
            '&Phone_list=' . urlencode($to),
            '&Message=' . urlencode(iconv('UTF-8', 'windows-1251', $msg))
        ])));

        preg_match("/message_id=([0-9]+)/isUu", $Response, $externalId);

        if (!empty($externalId) && !empty($externalId[1])) {
            return $externalId[1];
        }

        return 0;
    }

}

?>
