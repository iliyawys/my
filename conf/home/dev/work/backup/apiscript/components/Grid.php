<?php

namespace app\components;

use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\ActiveRecord;
use yii\web\HttpException;

class Grid
{
    private $_model = null;
    private $_modelName = '';
    private $_config = [];

    public static function regenerateByKey($Key, $Object)
    {
        if (!empty($Object))
        {
            $ToReturn = array();
            foreach($Object as $Value)
            {
                if (is_object($Value))
                    $ToReturn[$Value->$Key] = $Value;
                elseif (is_array($Value))
                    $ToReturn[$Value[$Key]] = $Value;
            }

            return $ToReturn;
        }
        else
            return false;
    }

    public static function regenerateByPk($Object) {
        return self::regenerateByKey('id', $Object);
    }

    public function init($ModelName = null, $Config = [])
    {
        $Model = (!is_null($ModelName) ? "app\\models\\" . $ModelName : null);

        if (!isset($Model)) {
            throw new HttpException(\Yii::t('base', 'model_error'));
        }
        $this->_modelName = $Model;
        $this->setModel(new $Model);
        $this->_config = $Config;


        return $this;
    }

    public function getModel() {
        return $this->_model;
    }

    public function setModel(ActiveRecord $Model) {
        $this->_model = $Model;
    }

    public function getColumns() {
        return \Yii::$app->params['grid'][$this->_modelName];
    }

    public function getSort()
    {
        $SessionKey = 'ModelSort_'.$this->_modelName;
        $GETSortKey = \Yii::$app->request->get('sortBy', '');
        $GETSortValue = \Yii::$app->request->get('sortValue', '');
        $Value = null;

        if ($GETSortKey != '' && $GETSortValue != '') {
            $_SESSION[$SessionKey] = [$GETSortKey, $GETSortValue];
        }

        if (!isset($_SESSION[$SessionKey])) {
            foreach($this->getColumns() as $Key=>$Config) {
                if (isset($Config['defaultSort']))
                {
                    if (is_callable($Config['defaultSort'])) {
                        $_SESSION[$SessionKey] = $Config['defaultSort']();
                    } else {
                        $_SESSION[$SessionKey] = $Config['defaultSort'];
                    }
                }
            }
        }

        if (!isset($_SESSION[$SessionKey])) {
            $_SESSION[$SessionKey] = ['id', 'DESC'];
        }

        return $_SESSION[$SessionKey];
    }

    public function getFilterValue($Key)
    {
        $SessionKey = 'Model_'.$this->_modelName;
        $GETValue = \Yii::$app->request->get($Key, '');
        $Value = null;

        if ($GETValue != '') {
            $Value = $GETValue;
            $_SESSION[$SessionKey][$Key] = $Value;
        } elseif (isset($_GET[$Key])) {
            unset($_SESSION[$SessionKey][$Key]);
        } elseif (isset($_SESSION[$SessionKey][$Key])) {
            $Value = $_SESSION[$SessionKey][$Key];
        } elseif (isset($this->getColumns()[$Key]['filter']['default'])) {
            $Value = $this->getColumns()[$Key]['filter']['default'];
        }

        if (isset($FuncName)) {
            $FuncName = $this->getColumns()[$Key]['filter']['value'];
            if (is_callable($FuncName)) {
                $Value = $FuncName($Value);
            }
        }

        return $Value;
    }

    public function getCurrentPage() {
        $SessionKey = 'ModelPage_'.$this->_modelName;
        $GETValue = \Yii::$app->request->get('page', '');

        if ((int)$GETValue > 1) {
            $_SESSION[$SessionKey] = (int)$GETValue;
        } else {
            $_SESSION[$SessionKey] = 1;
        }

        return $_SESSION[$SessionKey];
    }

    public function getData($AndWhere = '') {
        $Columns = $this->getColumns();

        $Query = $this->getModel()->find();
        // echo $this->_modelName;die();
        // if($this->_modelName=='app\models\Trunk'){
            // $Query->with('groups');
            // echo 5;die();
        // }

        $Sort = $this->getSort();

        $Params = [];
        foreach($Columns as $Key=>$Column) {
            $Params[$Key] = $this->getFilterValue($Key);

            if (isset($Column['filter']['search'])) {
                if ($Column['filter']['search'] !== false)
                {
                    if (is_callable($Column['filter']['search'])) {
                        $Column['filter']['search']($Query, $Params[$Key]);
                    } else {
                        $Query->andFilterWhere(['like', $this->getModel()->tableName().'.'.$Key, $Params[$Key]]);
                    }
                }
            } else {
                $Query->andFilterWhere([$this->getModel()->tableName().'.'.$Key => $Params[$Key]]);
            }

            if (isset($Column['filter']['sort'])) {
                if ($Sort[0] == $Key) {
                    if (is_callable($Column['filter']['sort'])) {
                        $Column['filter']['sort']($Query, $Sort[1]);
                    } else {
                        if ($Sort[1] == 'ASC') {
                            $Query->orderBy($this->getModel()->tableName().'.'.$Key . ' ASC');
                        } else {
                            $Query->orderBy($this->getModel()->tableName().'.'.$Key . ' DESC');
                        }
                    }
                }
            }
        }

        $CurrentPage = $this->getCurrentPage();
        $Query->limit = isset($this->_config['limit']) ? $this->_config['limit'] : 7;
        $Query->offset = $Query->limit * ( $CurrentPage - 1 );

        if ($AndWhere != '') {
            $Query->andWhere($AndWhere);
        }

        $Pages = false;
        if ((int)$Query->limit > 0 && !isset($this->_config['noPagination'])) {
            $Pages = new Pagination([
                'totalCount' => $Query->count(),
                'page' => $CurrentPage-1,
                'defaultPageSize' => isset($this->_config['limit']) ? $this->_config['limit'] : 7,
                'params' => [],
                'route' => $this->getBackendUrl()
            ]);
        }
        $Items = $Query
            ->all();

        return [
            'items' => $Items,
            'pages' => $Pages
        ];
    }


    /* TODO: скорее всего есть метод, не нашел */
    public function getBackendUrl() {
        return str_replace(
            str_replace('/controllers','', \Yii::$app->controllerPath),
            '',
            str_replace('/views', '', \Yii::$app->controller->getViewPath())
        );
    }

    public function getTableName() {
        return str_replace(\Yii::$app->db->tablePrefix, '', $this->getModel()->getTableSchema()->name);
    }
}

?>