<?php

    namespace app\components;

    class Helper
    {
        private static $key = 'asdqwe822232asiquwysjrhd';

        public static function fnEncrypt($sValue)
        {
            return rtrim(
                base64_encode(
                    mcrypt_encrypt(
                        MCRYPT_RIJNDAEL_256,
                        self::$key, $sValue,
                        MCRYPT_MODE_ECB,
                        mcrypt_create_iv(
                            mcrypt_get_iv_size(
                                MCRYPT_RIJNDAEL_256,
                                MCRYPT_MODE_ECB
                            ),
                            MCRYPT_RAND)
                    )
                ), "\0"
            );
        }

        public static function fnDecrypt($sValue)
        {
            return rtrim(
                mcrypt_decrypt(
                    MCRYPT_RIJNDAEL_256,
                    self::$key,
                    base64_decode($sValue),
                    MCRYPT_MODE_ECB,
                    mcrypt_create_iv(
                        mcrypt_get_iv_size(
                            MCRYPT_RIJNDAEL_256,
                            MCRYPT_MODE_ECB
                        ),
                        MCRYPT_RAND
                    )
                ), "\0"
            );
        }
    }

?>