<?php

    namespace app\components;

    class Log
    {
        public static function input($FileName = 'input.log')
        {
            file_put_contents(\Yii::getAlias('@runtime') . '/logs/' . $FileName, implode("\r\n", [
                "TIMESTAMP: " . date('d.m.Y H:i:s'),
                "IP: " . $_SERVER['REMOTE_ADDR'],
                "QS: " .$_SERVER["QUERY_STRING"],
                "GET: " . serialize($_GET),
                "POST: " . serialize($_POST)
            ]) . "\r\n\r\n", FILE_APPEND);
        }
    }

?>