<?php

    namespace app\components;

    class Sidebar
    {
        public function isActive($Array = ['']) {
            $isActive = false;

            $CurrentPath = preg_replace('/\?.*/is', '', ltrim($_SERVER['REQUEST_URI'], '/'));

            if ($CurrentPath == '/') {
                if ($Array == ['']) {
                    $isActive = true;
                }
            } else {
                $Parts = explode("/", $CurrentPath);
                if ($Array == $Parts) {
                    $isActive = true;
                } else {
                    $isActiveTest = true;
                    foreach($Array as $Key=>$Part) {
                        if (!isset($Parts[$Key]) || $Parts[$Key] != $Part) {
                            $isActiveTest = false;
                        }
                    }
                    $isActive = $isActiveTest;
                }
            }

            if (isset($_COOKIE['sa_' . implode("/", $Array)])) {
                $isActive = true;
            }

            return $isActive;
        }
    }

?>