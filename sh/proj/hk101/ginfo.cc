
#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x)) 
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))


int getName(SOCKET s){
	setlocale(LC_CTYPE, "rus"); 
	char ttmp[100];
	FIXED_INFO *pFixedInfo;
	ULONG ulOutBufLen;
	DWORD dwRetVal;
	IP_ADDR_STRING *pIPAddr;

	pFixedInfo = (FIXED_INFO *) MALLOC(sizeof (FIXED_INFO));
	if (pFixedInfo == NULL) {
	    wsprintf(ttmp,"\tempty name\n");
		send(s, ttmp, strlen(ttmp), 0);
	    return 0;
	}
	ulOutBufLen = sizeof (FIXED_INFO);


	if (GetNetworkParams(pFixedInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW) {
	    free(pFixedInfo);
	    pFixedInfo = (FIXED_INFO *) malloc(ulOutBufLen);
	    if (pFixedInfo == NULL) {
	        wsprintf(ttmp,"\tno name\n");
			// strcat(r,ttmp);
	    }
	}
	wsprintf(ttmp,"%s", pFixedInfo->HostName);
	printf("%s\n",ttmp);
	send(s, ttmp, strlen(ttmp), 0);
}

int sysinf(SOCKET s){
	setlocale(LC_CTYPE, "rus"); 
	char r[10240];
	char ttmp[100];
	memset(r,'\0',10240);

	SYSTEM_INFO siSysInfo;
 	GetSystemInfo(&siSysInfo); 

	strcat(r, "\n\tHardware information: \n");  
	
	wsprintf(ttmp, "   OEM ID: \t\t\t%u\n", siSysInfo.dwOemId);
	strcat(r,ttmp);
	wsprintf(ttmp, "   Number of processors: \t%u\n", 
	siSysInfo.dwNumberOfProcessors); 
	strcat(r,ttmp);
	wsprintf(ttmp,"   Page size: \t\t\t%u\n", siSysInfo.dwPageSize); 
	strcat(r,ttmp);
	wsprintf(ttmp,"   Processor type: \t\t%u\n", siSysInfo.dwProcessorType); 
	strcat(r,ttmp);
	wsprintf(ttmp,"   Minimum application address: %lx\n", 
	siSysInfo.lpMinimumApplicationAddress); 
	strcat(r,ttmp);
	wsprintf(ttmp,"   Maximum application address: %lx\n", 
	siSysInfo.lpMaximumApplicationAddress); 
	strcat(r,ttmp);
	wsprintf(ttmp,"   Active processor mask: \t%u\n", 
	siSysInfo.dwActiveProcessorMask); 
	strcat(r,ttmp);

	strcat(r, "\tNetwork information: \n");  

	
	FIXED_INFO *pFixedInfo;
	ULONG ulOutBufLen;
	DWORD dwRetVal;
	IP_ADDR_STRING *pIPAddr;

	pFixedInfo = (FIXED_INFO *) MALLOC(sizeof (FIXED_INFO));
	if (pFixedInfo == NULL) {
	    wsprintf(ttmp,"\tError allocating memory needed to call GetNetworkParams\n");
		strcat(r,ttmp);
		send(s, r, strlen(r), 0);
	    return 0;
	}
	ulOutBufLen = sizeof (FIXED_INFO);


	if (GetNetworkParams(pFixedInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW) {
	    free(pFixedInfo);
	    pFixedInfo = (FIXED_INFO *) malloc(ulOutBufLen);
	    if (pFixedInfo == NULL) {
	        wsprintf(ttmp,"\tError allocating memory needed to call GetNetworkParams\n");
			strcat(r,ttmp);
	    }
	}
	
	wsprintf(ttmp,"   Host Name: %s\n", pFixedInfo->HostName);
	strcat(r,ttmp);
	wsprintf(ttmp,"   Domain Name: %s\n", pFixedInfo->DomainName);
	strcat(r,ttmp);
	wsprintf(ttmp,"   DNS Servers:\n");
	strcat(r,ttmp);
	wsprintf(ttmp,"\t%s\n", pFixedInfo->DnsServerList.IpAddress.String);

	pIPAddr = pFixedInfo->DnsServerList.Next;
	while (pIPAddr) {
	    wsprintf(ttmp,"\t-%s\n", pIPAddr->IpAddress.String);
		strcat(r,ttmp);
	    pIPAddr = pIPAddr->Next;
	}

	wsprintf(ttmp,"   Node Type: ");
	strcat(r,ttmp);
	switch (pFixedInfo->NodeType) {
	  	case 1:
	        wsprintf(ttmp,"\t%s\n", "Broadcast");
	        break;
	    case 2:
	        wsprintf(ttmp,"\t%s\n", "Peer to peer");
	        break;
	    case 4:
	        wsprintf(ttmp,"\t%s\n", "Mixed");
	        break;
	    case 8:
	        wsprintf(ttmp,"\t%s\n", "Hybrid");
	        break;
	    default:
	        wsprintf(ttmp,"\n");
    }
	strcat(r,ttmp);
	
	wsprintf(ttmp,"   NetBIOS Scope ID: %s\n", pFixedInfo->ScopeId);
	strcat(r,ttmp);

	if (pFixedInfo->EnableRouting)
	    wsprintf(ttmp,"   IP Routing Enabled: Yes\n");
	else
	    wsprintf(ttmp,"   IP Routing Enabled: No\n");
	strcat(r,ttmp);

	if (pFixedInfo->EnableProxy)
	    wsprintf(ttmp,"   WINS Proxy Enabled: Yes\n");
	else
	    wsprintf(ttmp,"   WINS Proxy Enabled: No\n");
	strcat(r,ttmp);

	if (pFixedInfo->EnableDns)
	    wsprintf(ttmp,"   NetBIOS Resolution Uses DNS: Yes\n");
	else
	    wsprintf(ttmp,"   NetBIOS Resolution Uses DNS: No\n");
	strcat(r,ttmp);
	// return r;
	
	send(s, r, strlen(r), 0);
	
	return 1;
}

