#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <time.h>

#include <ctype.h>

#include <readline/readline.h>
#include <readline/history.h>

const char *port = "4000";
// using namespace std; 

/* We will use this struct to pass parameters to one of the threads */
struct workerArgs
{
	int 	socket;
	char *	ip;
};


pthread_t server_thread;

void *parseCmd(void *args);
void *accept_clients(void *args);
void *service_single_client(void *args);

void *parseCmd(void *args){
	
	int setServ  = 0;
	int nsize = 0;
	char buff[1024];
    bzero(buff,1024);

    char name[1024];

	while(1){
		printf("(%d) %s ", setServ, name);
		char * line = readline(">>> ");
	    if(!line) break;
	    if(*line) add_history(line);
	    if(strlen(line)==0){
	        printf("%s\n", "Command empty");
	        continue;
	    }
	    if( strcmp (line, "exit") == 0){
                printf("%s\n", "Exit server script");
                exit(1);
        }
        if( strcmp (line, "list") == 0){
                printf("%s\n", "listing clients");
                int i = 1;
                while(1){
                	if(i>200)
                		break;
                	char gn[] = "gn";
        			int cnt=send(i,gn,strlen(gn),0);
        			nsize = recv(i, &buff[0], sizeof(buff) - 1, 0);
        			if(nsize > 0){
                		buff[nsize] = '\0'; 
                		printf("<<< (%d) %s\n", i, buff);
        			}
                	i++;
                }
                continue;
        }

        if( strncmp (line, "set", 3) == 0){
        	setServ = atoi(strtok(line,"set "));
        	char gn[] = "gn";
        	int cnt=send(setServ,gn,strlen(gn),0);
        	nsize = recv(setServ, &buff[0], sizeof(buff) - 1, 0);
        	if(nsize > 0){
                buff[nsize] = '\0'; 
                name[nsize] = '\0'; 
                strcpy (name,buff);
        	}
            continue;
        }

        int cnt=send(setServ,line,strlen(line),0);
        nsize = recv(setServ, &buff[0], sizeof(buff) - 1, 0);

        if(nsize > 0){
                buff[nsize] = '\0'; 
                printf("<<< %s\n", buff);
        }

        free(line);
	}
}

int main(int argc, char *argv[]){

	pthread_t cmd_thread;

	sigset_t new;
	sigemptyset (&new);
	sigaddset(&new, SIGPIPE);

	if (pthread_sigmask(SIG_BLOCK, &new, NULL) != 0) {
		perror("Unable to mask SIGPIPE");
		exit(-1);
	}

	if (pthread_create(&server_thread, NULL, accept_clients, NULL) < 0){
		perror("Could not create server thread");
		exit(-1);
	}

	if(pthread_create(&cmd_thread, NULL, parseCmd, NULL) < 0){
    	perror("Could not create command thread");
		exit(-1);
    }

	pthread_join(cmd_thread, NULL);
	pthread_join(server_thread, NULL);
	pthread_exit(NULL);

}

void *accept_clients(void *args){
	int serverSocket;
	int clientSocket;
	pthread_t worker_thread;
	struct addrinfo hints, *res, *p;
	// struct sockaddr_storage *clientAddr;
	struct sockaddr_in *clientAddr;

	socklen_t sinSize = sizeof(struct sockaddr_storage);

	struct workerArgs *wa;
	int yes = 1;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // Return my address, so I can bind() to it

	/* Note how we call getaddrinfo with the host parameter set to NULL */
	if (getaddrinfo(NULL, port, &hints, &res) != 0){
		perror("getaddrinfo() failed");
		pthread_exit(NULL);
	}

	for(p = res;p != NULL; p = p->ai_next) {
		if ((serverSocket = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("Could not open socket");
			continue;
		}

		if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1){
			perror("Socket setsockopt() failed");
			close(serverSocket);
			continue;
		}

		if (bind(serverSocket, p->ai_addr, p->ai_addrlen) == -1){
			perror("Socket bind() failed");
			close(serverSocket);
			continue;
		}

		if (listen(serverSocket, 5) == -1){
			perror("Socket listen() failed");
			close(serverSocket);
			continue;
		}

		break;
	}
	
	freeaddrinfo(res);

	if (p == NULL){
    		fprintf(stderr, "Could not find a socket to bind to.\n");
		pthread_exit(NULL);
	}

	/* Loop and wait for connections */
	while (1){

		/* Call accept(). The thread will block until a client establishes a connection. */
		clientAddr = malloc(sinSize);
		if ((clientSocket = accept(serverSocket, (struct sockaddr *) clientAddr, &sinSize)) == -1) {
			free(clientAddr);
			perror("Could not accept() connection");
			continue;
		}
		wa = malloc(sizeof(struct workerArgs));
		wa->socket 	= clientSocket;
		wa->ip		= inet_ntoa(clientAddr->sin_addr);

		if (pthread_create(&worker_thread, NULL, service_single_client, wa) != 0) {
			perror("Could not create a worker thread");
			free(clientAddr);
			free(wa);
			close(clientSocket);
			close(serverSocket);
			pthread_exit(NULL);
		}
	}

	pthread_exit(NULL);
}

void *service_single_client(void *args) {
	struct workerArgs *wa;
	int socket, nbytes;
	char tosend[100];
	char name[100];
	char * ip;

	wa = (struct workerArgs*) args;
	socket 	= wa->socket;
 	ip		= wa->ip;
	
	pthread_detach(pthread_self());

	fprintf(stderr, "\nSocket ip: %s (%d) connected",ip, socket);
	printf("\t%s", " >>> ");

	while(1){
		// sprintf(tosend,"%d -- Hello, socket! %d\n", time(NULL), socket);
		// nbytes = send(socket, tosend, strlen(tosend), 0);

		if (nbytes == -1 && (errno == ECONNRESET || errno == EPIPE)){
			fprintf(stderr, "\nSocket: %s (%d) disconnected\n", ip, socket);
			printf("\t%s", " >>> ");
			close(socket);
			free(wa);
			pthread_exit(NULL);
		}else if (nbytes == -1){
			perror("Unexpected error in send()");
			free(wa);
			pthread_exit(NULL);
		}
		sleep(5);
	}

	pthread_exit(NULL);
}