// #include <iostream.h>
// #include <fstream.h>
// #include <cstdlib.h>

#include <iconv.h>

char iconv_recode(const char from, const  char to, char *text)
{
    iconv_t cnv = iconv_open(to, from);

    if (cnv == (iconv_t) - 1) {
        iconv_close(cnv);
        return "";
    }

    char *outbuf;
    if ((outbuf = (char *) malloc(text.length()*2 + 1)) == NULL) {
        iconv_close(cnv);
        return "";
    }

    char *ip = (char *) text, *op = outbuf;
    size_t icount = sizeof(text), ocount = sizeof(text)*2;

    if (iconv(cnv, &ip, &icount, &op, &ocount) != (size_t) - 1) {
        outbuf[sizeof(text)*2 - ocount] = '\0';
        text = outbuf;
    } else {
        text = "";
    }

    free(outbuf);
    iconv_close(cnv);

    return text;
}
