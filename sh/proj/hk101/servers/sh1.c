#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include <ctype.h>
#include <locale.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "enc.c"

const char *port = "4000";

struct workerArgs
{
	int 	socket;
	char *	ip;
};
/*
	cmd:
	
	list 			-> list all sockets
	set 5	 		-> set socket number 5
	ls <path>		-> list dir
	cp <path>		-> copy client to srv (folder ./upload/)
	-up <path><path>-> upload file <srv dir> <client dir>
	start 			-> execute command
	rl 				-> reload client
	ii 				-> info about client
	quit 			-> exit client
	exit 			-> exit server 
*/

pthread_t server_thread;

char *utfd(char *buff);
void *parseCmd(void *args);
void *accept_clients(void *args);
void *service_single_client(void *args);

char *utfd(char *buff){
	char *alp 		= "йцукенгшщзхъфывапролджэюбьтимсчяЯЧСМИТЬБЮЭЖДЛОРПАВЫФЙЦУКЕНГШЩЗХЪ";
	int codes[] 	= {
		-23,-10,-13,-22,-27,-19,-29,-8,-7,-25,-11,-6,-12,-5,-30,-32,
		-17,-16,-18,-21,-28,-26,-3,-2,-31,-4,-14,-24,-20,-15,-9,-1,
		-33,-41,-47,-52,-56,-46,-36,-63,-34,-35,-58,-60,-53,-50,-48,
		-49,-64,-62,-37,-44,-55,-42,-45,-54,-59,-51,-61,-40,-39,-57,-43,-38
	};

	int i = 0;
	char *result = malloc(10240);
	bzero(result,10240);
	result[0]='\0';
	int count = 0;

	while(i<strlen(buff)){
		int sign = buff[i];
		int ii = 0;
		while(ii<64){
			if(sign == codes[ii]){
				int c1 = ii * 2;
				int c2 = c1 + 1;
				result[count] = alp[c1];
				count++;
				result[count] = alp[c2];
				count++;
				break;
			}
			ii++;
		}
		if(ii==64){
			result[count]=buff[i];
			count++;
		}
		i++;
	}
	result[count+1]=0;
	return result;
}

void *parseCmd(void *args){	
	int setServ  = 0;
	int nsize = 0;
    char nameServ[64] = "noset";
    char *result = malloc(10240);

	while(1){
		char buff[1024];
    	bzero(buff,1024);

		bzero(result,10240);


		printf("(%d) %s\t", setServ, nameServ);
		char * line = readline(" >>> ");
	    if(!line) break;
	    if(*line) add_history(line);
	    if(strlen(line)==0){
	        continue;
	    }
	    if( strcmp (line, "exit") == 0){
                printf("%s\n", "Exit server script");
                exit(1);
        }
        if( strcmp (line, "list") == 0){
                printf("%s\n", "listing clients");
                int i = 1;
                while(1){
                	if(i>200)
                		break;
                	char *gn = "gn";
        			int cnt=send(i,gn,strlen(gn),0);
        			nsize = recv(i, &buff[0], sizeof(buff) - 1, 0);
        			result = utfd(buff);
        			if(nsize > 0){
                		buff[nsize] = '\0'; 
                		printf("<<< (%d) %s\n", i, result);
        			}
                	i++;
                }
                continue;
        }
        /* set socket */
        if( strncmp (line, "set", 3) == 0){
        	setServ = atoi(strtok(line,"set "));
        	char *gn = "gn";
        	int cnt=send(setServ,gn,strlen(gn),0);
        	nsize = recv(setServ, &buff[0], sizeof(buff) - 1, 0);
        	if(nsize > 0){
                buff[nsize] = '\0'; 
                nameServ[nsize] = '\0'; 
        		result = utfd(buff);
                bzero(nameServ,64);
                strcpy (nameServ,result);
                swprintf(nameServ,L"%s",result);
        	}else{
        		strcpy (nameServ,"noset");
        	}
            continue;
        }

        /* download */
        if( strncmp (line, "cp", 2) == 0){
        	/*
             	const char* d = strtok(line,"cp ");
            	send(setServ,line,strlen(line),0);
                printf("\tReceiving file .. \n");

                int Size;
                char Filesize[1024] = "";
                char *filename[1024];

                nsize = recv(setServ, Filesize, 1024, 0);
                if( strcmp (Filesize, "nofound") == 0){
                    printf("%s\n", "No found");
                    continue;
                }
                if(nsize > 0) {
                    Size = atoi((const char*)Filesize);
                    printf("File size: %d\n", Size);
                    printf("%s\n", Filesize);
                }
                
                int  fSize = 0;
                nsize = recv(setServ, &buff[0], sizeof(buff) - 1, 0);
                fSize = nsize + 9;

                char fName[fSize];
                bzero(fName,fSize);
                if(nsize > 0){
                    buff[nsize] = '\0'; 
                    strcat(fName, "./upload/");
                    strcat(fName, buff);
                    printf("filename: %s\n", fName);
                }else{
                    continue;
                }
    			
    			bzero(buff,1024);
                char *Buffer[Size];
                printf("%s\n", nameServ);
                if(recv(setServ, Buffer, Size, 0)) {
                    FILE *uploadF;
                	// printf("1--%s-%d-%d\n", fName, sizeof(fName), fSize);
                    uploadF = fopen(fName, "wb");
                	// printf("2-%s-\n", nameServ);
                    fwrite((const char*)Buffer, 1, Size, uploadF);
                    fclose(uploadF);
                    bzero(Buffer,Size);

                }
                printf("%s\n", "End transfering");
                // printf("%s\n", nameServ);
            */
                continue;
        }

        /* upload */
        // if( strncmp (line, "ul", 2) == 0){}

        int cnt=send(setServ,line,strlen(line),0);
        nsize = recv(setServ, &buff[0], sizeof(buff) - 1, 0);

        if(nsize > 0){
                buff[nsize] = '\0'; 
               	result = utfd(buff);
                printf("<<< %s\n", result);
               	// char *dd = utfd(buff);
        }
        free(line);
	}
}



int main(int argc, char *argv[]){
	
	setlocale(LC_CTYPE, "UTF-8");

	/*while(1){
		char * line = readline(" >>> ");
	    if(!line) break;
	    if(*line) add_history(line);
	    if(strlen(line)==0){
	        continue;
	    }
	    char *type = enc(line);
	    printf("enc - %s\n", type);
	    printf("dec - %s\n", dec(type));
	    // free(type);
		// printf("%s\n",enc(line));
	}
	exit(0);
	*/
	pthread_t cmd_thread;

	sigset_t new;
	sigemptyset (&new);
	sigaddset(&new, SIGPIPE);

	if (pthread_sigmask(SIG_BLOCK, &new, NULL) != 0) {
		perror("Unable to mask SIGPIPE");exit(-1);
	}

	if (pthread_create(&server_thread, NULL, accept_clients, NULL) < 0){
		perror("Could not create server thread");exit(-1);
	}

	if(pthread_create(&cmd_thread, NULL, parseCmd, NULL) < 0){
    	perror("Could not create command thread");exit(-1);
    }

	pthread_join(cmd_thread, NULL);
	pthread_join(server_thread, NULL);
	pthread_exit(NULL);

}

void *accept_clients(void *args){
	int serverSocket;
	int clientSocket;
	pthread_t worker_thread;
	struct addrinfo hints, *res, *p;
	// struct sockaddr_storage *clientAddr;
	struct sockaddr_in *clientAddr;

	socklen_t sinSize = sizeof(struct sockaddr_storage);

	struct workerArgs *wa;
	int yes = 1;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // Return my address, so I can bind() to it

	if (getaddrinfo(NULL, port, &hints, &res) != 0){
		perror("getaddrinfo() failed");pthread_exit(NULL);
	}

	for(p = res;p != NULL; p = p->ai_next) {
		if ((serverSocket = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("Could not open socket");continue;
		}

		if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1){
			perror("Socket setsockopt() failed");close(serverSocket);continue;
		}

		if (bind(serverSocket, p->ai_addr, p->ai_addrlen) == -1){
			perror("Socket bind() failed");
			close(serverSocket);continue;
		}

		if (listen(serverSocket, 5) == -1){
			perror("Socket listen() failed");
			close(serverSocket);continue;
		}
		break;
	}
	
	freeaddrinfo(res);

	if (p == NULL){
    	fprintf(stderr, "Could not find a socket to bind to.\n");pthread_exit(NULL);
	}

	while (1){
		clientAddr = malloc(sinSize);
		if ((clientSocket = accept(serverSocket, (struct sockaddr *) clientAddr, &sinSize)) == -1) {
			free(clientAddr);perror("Could not accept() connection");
			continue;
		}
		wa = malloc(sizeof(struct workerArgs));
		wa->socket 	= clientSocket;
		wa->ip		= inet_ntoa(clientAddr->sin_addr);

		if (pthread_create(&worker_thread, NULL, service_single_client, wa) != 0) {
			perror("Could not create a worker thread");
			free(clientAddr);free(wa);
			close(clientSocket);
			close(serverSocket);
			pthread_exit(NULL);
		}
	}
	pthread_exit(NULL);
}

void *service_single_client(void *args) {
	
	struct workerArgs *wa;
	int socket, nbytes;
	char tosend[100];
	char name[100];
	char * ip;

	wa = (struct workerArgs*) args;
	socket 	= wa->socket;
 	ip		= wa->ip;
	
	pthread_detach(pthread_self());

	fprintf(stderr, "\nSocket ip: %s (%d) connected\n",ip, socket);
	fprintf(stderr, "\t\t%s", " >>> ");

	while(1){
		// sprintf(tosend,"%d\n", time(NULL), socket);
		// nbytes = send(socket, tosend, strlen(tosend), 0);
		if (errno == ECONNRESET || errno == EPIPE){
			fprintf(stderr, "\nSocket: %s (%d) disconnected\n", ip, socket);
			fprintf(stderr, "\t\t%s", " >>> ");
			close(socket);free(wa);
			pthread_exit(NULL);
		}else if (nbytes == -1){
			perror("Unexpected error in send()");
			free(wa);pthread_exit(NULL);
		}
		sleep(5);
	}
	pthread_exit(NULL);
}
