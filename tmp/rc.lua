
-- Standard awesome library
require("awful")
require("awful.autofocus")
require("awful.rules")
-- Theme handling library
require("beautiful")
-- Notification library
require("naughty")
naughty.config.presets.normal.opacity = 0.8
naughty.config.presets.low.opacity = 0.8
naughty.config.presets.critical.opacity = 0.8
-- Load Debian menu entries
require("debian.menu")

require("vicious")
-- require("blingbling.task_warrior")
-- require("awesome.taskwarrior")
-- require("getrss")
-- awful.util.spawn_with_shell("unagi &")
-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then    
  naughty.notify({ preset = naughty.config.presets.critical,
    title = "Oops, there were errors during startup!",
    text = awesome.startup_errors })
end
-- Handle runtime errors after startup
do
  local in_error = false
  awesome.add_signal("debug::error", function (err)
    -- Make sure we don't go into an endless error loop
    if in_error then return end
    in_error = true
    naughty.notify({ preset = naughty.config.presets.critical,
                  title = "Oops, an error happened!",
                  text = err })
    in_error = false
  end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
-- beautiful.init("/home/dev/.config/awesome/default/theme.lua")
-- This is used later as the default terminal and editor to run.
terminal = "urxvt"
editor = os.getenv("EDITOR") or "editor"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
 modkey = "Mod4"
 -- modkey = "Mod1"

-- Table of layouts to cover with awful.layout.inc, order matters.
layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier
}
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}
-- for s = 1, screen.count() do
    tags[1] = awful.tag({ " HOME ", " WORK ", " IM "," ETC " }, 1, 
                        {layouts[3],layouts[5],layouts[5],}
      )
    tags[1] = awful.tag({ " WWW ", " LOG ", " ETC " }, 1, 
                        {layouts[3],layouts[5],layouts[5],}
      )
    awful.tag.setproperty(tags[1][1], "mwfact", 0.60)
    -- awful.tag.setproperty(tags[s][2], "mwfact", 0.60)
-- end
-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu
myawesomemenu = {
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", awesome.quit }
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "Debian", debian.menu.Debian_menu.Debian },
                                    { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = image(beautiful.awesome_icon),
                                     menu = mymainmenu })
-- }}}
-- {{{ Wibox

datewidget = widget({ type = "textbox" })
vicious.register(datewidget, vicious.widgets.date, " %d-%m-%Y %R ")

-- Create a systray
mysystray = widget({ type = "systray" })

-- Create a wibox for each screen and add it
mywibox = {}
mybottombox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, awful.tag.viewnext),
                    awful.button({ }, 5, awful.tag.viewprev)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
  -- Create a promptbox for each screen
  mypromptbox[s] = awful.widget.prompt({ layout = awful.widget.layout.horizontal.leftright })
  -- Create an imagebox widget which will contains an icon indicating which layout we're using.
  -- We need one layoutbox per screen.
  mylayoutbox[s] = awful.widget.layoutbox(s)
  mylayoutbox[s]:buttons(awful.util.table.join(
                        awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                        awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                        awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                        awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
  -- Create a taglist widget
  mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.label.all, mytaglist.buttons)
  -- Create a tasklist widget
  mytasklist[s] = awful.widget.tasklist(function(c)
                                            return awful.widget.tasklist.label.currenttags(c, s)
                                        end, mytasklist.buttons)
  -- Create the wibox
  mywibox[s] = awful.wibox({ position = "top", screen = s })
  mybottombox[s] =  awful.wibox({ position = "bottom", screen = s })
 
  mybottombox[s].widgets = {
    -- empt,
    -- memwidget,
    -- separator,
    -- netwidget,
    -- separator,
    -- my_fs,
    -- separator,
    -- tb_moc,
    -- spearator,
    -- mytaskwarrior,
    layout = awful.widget.layout.horizontal.leftright
  }
  -- Add widgets to the wibox - order matters
  mywibox[s].widgets = {
    {
      mylauncher,
      mytaglist[s],
      mypromptbox[s],
      layout = awful.widget.layout.horizontal.leftright
    },
    mylayoutbox[s],
    datewidget,
    -- separator,
    -- brigh,
    -- separator,
    -- volumewidget,
    -- separator,
    -- batwidget,
    --kbdcfg,
      s == 1 and mysystray or nil,
        -- empt,
        mytasklist[s],
        layout = awful.widget.layout.horizontal.rightleft
  }
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
  awful.button({ }, 3, function () mymainmenu:toggle() end),
  awful.button({ }, 4, awful.tag.viewnext),
  awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
  awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
  
  awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
  awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

  awful.key({ modkey,           }, "j",
            function ()
              awful.client.focus.byidx( 1)
              if client.focus then client.focus:raise() end
            end),
  awful.key({ modkey,           }, "k",
          function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
          end),
  awful.key({ modkey,           }, "w", function () mymainmenu:show({keygrabber=true}) end),

  -- Layout manipulation
  awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
  awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
  awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
  awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
  awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
  awful.key({ modkey,           }, "Tab",
          function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
          end),

  -- Standard program
  awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
  awful.key({ modkey, "Control" }, "r", awesome.restart),
  awful.key({ modkey, "Shift"   }, "q", awesome.quit),

  awful.key({ modkey,           }, "i",     function () awful.tag.incmwfact( 0.05)    end),
  awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
  awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
  awful.key({ modkey, "Shift"   }, "i",     function () awful.tag.incnmaster(-1)      end),
  awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
  awful.key({ modkey, "Control" }, "i",     function () awful.tag.incncol(-1)         end),
  awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
  awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),
  
  -- My keys
  -- awful.key({ modkey, }, "g",     function() awful.util.spawn("mocp -G") end),
  -- awful.key({ modkey, }, "/",     function() awful.util.spawn("mocp -t repeat") end),
  -- awful.key({ modkey, }, "]",     function() awful.util.spawn("mocp -f") end),
  -- awful.key({ modkey, }, "[",     function() awful.util.spawn("mocp -r") end),
  -- awful.key({ modkey, }, "s",     function() awful.util.spawn("mocp -s") end),
  -- awful.key({ modkey, }, "p",     function() awful.util.spawn("mocp -p") end),
  -- awful.key({ modkey,           }, "t",     function() getRss() end),
  -- awful.key({ modkey, "Shift"   }, "t",     function() getTask() end),
  -- awful.key({ modkey },            "=",     taskwarrior.toggle_tasklist),
  -- awful.key({ modkey,  "Shift"  }, "-",     function() awful.util.spawn("amixer set PCM 9%-") end),
  -- awful.key({ modkey,  "Shift"  }, "=",     function() awful.util.spawn("amixer set PCM 9%+") end),
  -- awful.key({ modkey,  "Control" }, "-",     function() awful.util.spawn("brigh -") end),
  -- awful.key({ modkey,  "Control" }, "=",     function() awful.util.spawn("brigh +") end),
  -- awful.key({ modkey}, "c",     function() getCows() end),
  -- awful.key({ modkey}, "a",     function() getHelp() end),
  -- awful.key({ modkey,  "Shift" }, "n",     function() getNote() end),

  awful.key({ modkey, "Control" }, "n", awful.client.restore),
  -- Prompt
  awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),

  awful.key({ modkey }, "x",
            function ()
              awful.prompt.run({ prompt = "Run Lua code: " },
              mypromptbox[mouse.screen].widget,
              awful.util.eval, nil,
              awful.util.getdir("cache") .. "/history_eval")
            end)
  )

clientkeys = awful.util.table.join(
  awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
  awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
  awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
  awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
  awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
  awful.key({ modkey, "Shift"   }, "r",      function (c) c:redraw()                       end),
  awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
  awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
  awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Compute the maximum number of digit we need, limited to 9
keynumber = 0
for s = 1, screen.count() do
   keynumber = math.min(9, math.max(#tags[s], keynumber));
end

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, keynumber do
  globalkeys = awful.util.table.join(globalkeys,
            awful.key({ modkey }, "#" .. i + 9,
              function ()
                local screen = mouse.screen
                if tags[screen][i] then
                  awful.tag.viewonly(tags[screen][i])
              end
            end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    -- tags[s] = awful.tag({ " HOME ", " WORK ", " WIN ", " WWW_T ", " WWW_F ", " IM ", " ETC ", " LOG " }, s, layouts[3])

    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    
    { rule = { instance= "sublime_text" }, callback = awful.client.setslave},
    
    
    { rule = { instance = "sublime_text" },
      -- properties = {opacity = 0.95},
      
      callback = function(c)  
        c:tags({tags[1][1]})
      end
    },

    { rule = { class = "Firefox" }, 
      -- properties = {floating = true}, 
      callback = awful.client.setmaster},
    { rule = { class = "Firefox" },
      callback = function(c) c:tags({tags[2][1]}) end,
    },
    --1366 x 768
    -- 19x0 150x548
    { rule = { instance = "workterm" },
      properties = { 
        tag = tags[1][1], 
        floating = true,
        opacity = 0.92,
      },
      callback = awful.client.setslave
    },     
    { rule = { instance = "workterm" }, 
      callback = function (c)
            c:geometry({y=19, x=0, height=250,width=548})
      end
    },
    --19x547 
    { rule = { instance = "worklog" },
      properties = { 
        tag = tags[1][1], 
        floating = true,
        opacity = 0.97,
      },
      callback = awful.client.setslave
    }, 
    { rule = { instance = "worklog" }, 
      callback = function (c)
            c:geometry({y=19, x=546, height=250, width=822})
      end
    },

    { rule = { instance = "hometerm1" },
      properties = { 
        tag = tags[1][1], 
        floating = true,
        opacity = 0.92,
      },
      callback = awful.client.setslave
    },         
    { rule = { instance = "hometerm1" }, 
      callback = function (c)
            c:geometry({y=265, x=0, height=488,width=548})
      end
    },
    { rule = { instance = "hometerm2" },
      properties = { 
        tag = tags[1][1], 
        floating = true,
        opacity = 0.92,
      },
      callback = awful.client.setslave
    },         
    { rule = { instance = "hometerm2" }, 
      callback = function (c)
            c:geometry({y=265, x=546, height=488,width=822})
      end
    },
    -- { rule = { class = "VirtualBox" }, properties = { tag = tags[1][3], },callback = awful.client.setmaster},
    { rule = { instance = "irc" }, properties = { tag = tags[1][3], },callback = awful.client.setmaster},
    { rule = { instance = "rss" }, properties = { tag = tags[1][3], },callback = awful.client.setmaster},
    -- { rule = { instance = "Telegram" }, properties = { tag = tags[1][2], },callback = awful.client.setmaster},
    -- { rule = { instance = "mcabber" },  properties = { tag = tags[1][5], },callback = awful.client.setslave },
    -- { rule = { instance = "mocp" },  properties = { tag = tags[1][6], },callback = awful.client.setslave },
    -- { rule = { instance = "irssi" },    properties = { minimized = true, tag = tags[1][3], },callback = awful.client.setslave },
    -- { rule = { instance = "mutt" },     properties = { tag = tags[1][5], },callback = awful.client.setslave },
    { rule = { class = "Tor Browser" }, properties = { floating=true, tag = tags[1][4], },callback = awful.client.setmaster},
    
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.add_signal("manage", function (c, startup)
    -- Add a titlebar
    -- awful.titlebar.add(c, { modkey = modkey })

    -- Enable sloppy focus
    c:add_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end
end)

client.add_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.add_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- getCows()